CREATE TABLE documents (
    doc_hash BYTEA NOT NULL,
    created TIMESTAMP WITH TIME ZONE NOT NULL,
    status INT NOT NULL,
    account BYTEA NOT NULL,
    password TEXT,
    receipt_id BYTEA,
    block_no BIGINT);

CREATE UNIQUE INDEX idx_doc_hash ON documents(doc_hash);
CREATE UNIQUE INDEX idx_receipt ON documents(receipt_id);

CREATE OR REPLACE FUNCTION lookup_doc(target_hash BYTEA)
RETURNS SETOF documents
AS $$
    SELECT doc_hash, created, status, account, password, receipt_id, block_no
      FROM documents
     WHERE doc_hash = target_hash
     LIMIT 1;
$$ LANGUAGE 'sql';
