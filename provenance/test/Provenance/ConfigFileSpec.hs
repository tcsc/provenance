module Provenance.ConfigFileSpec (spec) where

import Data.String.QQ
import Test.Hspec
import qualified Network.Ethereum.ABI.Prim.Address as Address
import Network.URI
import Data.Maybe (fromJust)
import Provenance.ConfigFile as ConfigFile
import qualified Data.Yaml as Yaml

spec :: Spec
spec = describe "Config file parser" $ do
  it "Must parse a valid confg file" $ do
    let text = [s|
bindings:
  - 127.0.0.1:8080
  - :4321
contract_address: 0123456789ABCDEF0123456789ABCDEF01234567
postgres: postgresql://someone:secret@localhost/mydb
|]
    let expected = Config {
        _bindings = ["127.0.0.1:8080", ":4321"]
      , _contract = "0123456789ABCDEF0123456789ABCDEF01234567"
      , _postgresUrl = (fromJust . parseURI) "postgresql://someone:secret@localhost/mydb"
      }
    case ConfigFile.parse text of
      Right actual -> actual `shouldBe` expected
      Left err -> fail $ show err

  it "Must detect an invalid URL" $ do
    let text = [s|
 bindings: []
 contract_address: 0123456789ABCDEF0123456789ABCDEF01234567
 postgres: not actually a URI
|]
    let (Left err) = ConfigFile.parse text
    Yaml.prettyPrintParseException err `shouldContain` "narf"