{-# LANGUAGE TemplateHaskell #-}

module Provenance.ConfigFile where

import Control.Lens
import qualified Data.Text as T
import Data.Text (Text)
import qualified Data.Text.Encoding as Encoding
import qualified Data.Yaml as Yaml
import Data.Yaml (FromJSON(..), (.:))
import qualified Network.Ethereum.ABI.Prim as Ethereum
import Network.URI
import Data.Maybe

data Config = Config
  { _bindings :: [Text]
  , _contract :: Ethereum.Address
  , _postgresUrl :: URI
  } deriving (Eq, Show)

makeLenses ''Config

instance FromJSON Config where
  parseJSON = Yaml.withObject "Config" $ \obj ->
    Config
      <$> obj .: "bindings"
      <*> obj .: "contract_address"
      <*> ((obj .: "postgres") >>= uri "postgres must be a valid URL")

    where
      uri :: String -> String -> Yaml.Parser URI
      uri msg s =
        case parseURI s of
          Just v -> return v
          Nothing -> fail msg

parse :: Text -> Either Yaml.ParseException Config
parse text = Yaml.decodeEither' $ Encoding.encodeUtf8 text
