{-# LANGUAGE TemplateHaskell #-}

module Provenance.CLI
  ( Args(..)
  , configFile
  , parse
  ) where

import Options.Applicative
import Control.Lens
import Control.Lens.TH
import System.Log.Logger as Log

data Args = Args
  { _configFile :: FilePath
  , _logLevel :: Log.Priority
  } deriving (Eq, Show)
makeLenses ''Args

args :: Parser Args
args = Args
  <$> strOption ( long "config" <>
                  short 'c' <>
                  metavar "FILE" <>
                  help "Server configuration file")
  <*> flag Log.INFO Log.DEBUG (long "verbose" <>
                               short 'v' <>
                               help "Be more verbose")


parse :: IO Args
parse = execParser arguments
  where
    arguments :: ParserInfo Args
    arguments = info (args <**> helper) (fullDesc <> header describe)

    -- describe :: IO ()
    describe = "provenenace server"