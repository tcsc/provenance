pragma solidity ^0.4.19;

contract Owned {
    address owner;

    function Owned() public {
        owner = msg.sender;
    }

    modifier ownerOnly() {
        require(msg.sender == owner);
        _;
    }

}

contract Mortal is Owned {
    function kill() public {
        if (msg.sender == owner) {
            selfdestruct(owner);
        }
    }
}

contract Timestamper is Mortal {
    struct Document {
        bool isValue;
        uint block;
        address owner;
    }

    event Registration(bool success, bytes32 doc_hash);

    mapping (bytes32 => Document) documents;

    function register(bytes32 doc_hash) public {
        bool already_exists = documents[doc_hash].isValue;
        require(!already_exists);
        documents[doc_hash] = Document(true, block.number, msg.sender);
        emit Registration(!already_exists, doc_hash);
    }

    function check(bytes32 doc_hash) public view returns (bool, uint, address) {
        Document storage d = documents[doc_hash];
        return (d.isValue, d.block, d.owner);
    }
}