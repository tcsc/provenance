module Nav exposing (Route(..), parseUrl)

import Navigation
import UrlParser exposing (Parser, (</>), parseHash, s, map, string, top)


type Route
    = Register
    | DocumentList
    | Document String
    | Faq


docRoute : Parser (Route -> a) a
docRoute =
    UrlParser.oneOf
        [ map DocumentList top
        , map Document string
        ]


route : Parser (Route -> a) a
route =
    UrlParser.oneOf
        [ (s "doc" </> docRoute)
        , map DocumentList (s "doc")
        , map Faq (s "faq")
        , map Register top
        ]


parseUrl : Navigation.Location -> Maybe Route
parseUrl loc =
    parseHash route loc
