port module Ports exposing (..)

import FileReader exposing (FileContentArrayBuffer)


port hashFile : FileContentArrayBuffer -> Cmd a


port fileHashed : (String -> a) -> Sub a
