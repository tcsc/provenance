module MaybeExtra
    exposing
        ( isJust
        )


isJust : Maybe a -> Bool
isJust m =
    case m of
        Just _ ->
            True

        Nothing ->
            False
