module Main exposing (main)

import Debug
import Html exposing (..)
import Html.Attributes exposing (..)
import Maybe
import Navigation
import Components.Navbar exposing (navbar)
import Bootstrap.Navbar as Navbar
import Nav
import Page


type alias Model =
    { navbar : Navbar.State
    , page : Page.Model
    }


type Msg
    = UrlChange Navigation.Location
    | PageMsg Page.Msg
    | NavbarMsg Navbar.State


type Location
    = Register
    | DocumentInfo String


main : Program Never Model Msg
main =
    Navigation.program UrlChange
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Page.subscriptions model.page |> Sub.map PageMsg
        ]


init : Navigation.Location -> ( Model, Cmd Msg )
init loc =
    let
        ( navbarState, navbarCmd ) =
            Navbar.initialState NavbarMsg

        initialModel =
            { navbar = navbarState
            , page = Page.init
            }

        ( model, pageCmd ) = selectPage loc initialModel
    in
        ( model, Cmd.batch [ navbarCmd, pageCmd ] )

selectPage : Navigation.Location -> Model -> ( Model, Cmd Msg )
selectPage loc model =
    Nav.parseUrl loc
        |> Maybe.withDefault Nav.Register
        |> \p -> Page.select p model.page
        |> \( m, cmd ) ->
            ( { model | page = m }, Cmd.map PageMsg cmd )

deselectPage : Model -> ( Model, Cmd Msg )
deselectPage model =
    Page.deselect model.page
        |> \(m, cmd) ->
            ( { model | page = m }, Cmd.map PageMsg cmd )

contentView : Model -> Html Msg
contentView model =
    Page.view model.page |> Html.map PageMsg

role : String -> Attribute Msg
role =
    attribute "role"

view : Model -> Html Msg
view model =
    div []
        [ navbar model.navbar NavbarMsg
        , Html.main_ [role "main", class "container"]
            [ contentView model ]
        ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case Debug.log "Main" msg of
        NavbarMsg s ->
            ( { model | navbar = s }, Cmd.none )

        PageMsg p ->
            let
                ( new_page, cmd ) =
                    Page.update p model.page
            in
                ( { model | page = new_page }, Cmd.map PageMsg cmd )

        UrlChange loc ->
            deselectPage model
                |> \(m, deselectCmd) -> selectPage loc m
                |> \(m1, selectCmd) ->
                    (m1, Cmd.batch [deselectCmd, selectCmd])