module Page exposing (..)

import Html exposing (..)
import Page.Register as Register
import Page.DocStatus as DocStatus
import Nav


type Msg
    = RegisterMsg Register.Msg
    | DocStatusMsg DocStatus.Msg


type Page
    = RegisterPage
    | DocStatusPage

type Status
    = Busy
    | Idle

type alias Model =
    { activePage : Page
    , register : Register.Model
    , docStatus : DocStatus.Model
    }


init : Model
init =
    { activePage = RegisterPage
    , register = Register.init
    , docStatus = DocStatus.init
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        RegisterMsg pm ->
            let
                ( new_reg_state, cmd ) =
                    Register.update pm model.register
            in
                ( { model | register = new_reg_state }
                , Cmd.map RegisterMsg cmd
                )

        DocStatusMsg dsm ->
            let
                ( new_doc_state, cmd ) =
                    DocStatus.update dsm model.docStatus
            in
                ( { model | docStatus = new_doc_state }
                , Cmd.map DocStatusMsg cmd
                )


view : Model -> Html Msg
view model =
    case model.activePage of
        RegisterPage ->
            Register.view model.register
                |> Html.map RegisterMsg

        DocStatusPage ->
            DocStatus.view model.docStatus
                |> Html.map DocStatusMsg


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Register.subscriptions model.register
            |> Sub.map RegisterMsg
        ]


select : Nav.Route -> Model -> ( Model, Cmd Msg )
select route model =
    case route of
        Nav.Register ->
            ( { model | activePage = RegisterPage }, Cmd.none )

        Nav.Document hash ->
            { model | activePage = DocStatusPage }
                |> \m -> DocStatus.select hash m.docStatus
                |> \(dsm, cmd) -> ( { m | docStatus = dsm }
                                  , Cmd.map DocStatusMsg cmd
                                  )

        _ ->
            let _ = Debug.log "Unexpected route: " route
            in (model, Cmd.none)

{-| Notifies the active page that the app is navigating to a new page.
-}
deselect : Model -> ( Model, Cmd Msg )
deselect model =
    case model.activePage of
        DocStatusPage ->
            DocStatus.deselect model.docStatus
                |> \(m, cmd) -> ( { model | docStatus = m }
                                , Cmd.map DocStatusMsg cmd
                                )

        _ -> ( model, Cmd.none )