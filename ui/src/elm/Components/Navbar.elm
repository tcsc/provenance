module Components.Navbar exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Bootstrap.Navbar as Navbar
import String


navbar : Navbar.State -> (Navbar.State -> a) -> Html a
navbar navbarState messageCtor =
    Navbar.config messageCtor
        |> Navbar.withAnimation
        |> Navbar.brand [ href "#" ] [ text "Provenance" ]
        |> Navbar.dark
        |> Navbar.items
            [ Navbar.itemLink [ href "#" ] [ text "home" ]
            , Navbar.itemLink [ href "#" ] [ text "about" ]
            ]
        |> Navbar.view navbarState
