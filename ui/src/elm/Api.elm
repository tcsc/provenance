module Api
    exposing
        ( DocumentReport
        , DocumentStatus (..)
        , registerDocument
        , getDocument
        , decodeDocumentReport
        , emptyDocumentReport
        , documentStatus
        )

import Http
import Maybe
import Json.Decode as Json
import Date


baseUri : String
baseUri =
    "/api/"


type DocumentStatus
    = DocRegistered
    | DocPending
    | DocSubmitted


type alias DocumentReport =
    { status : DocumentStatus
    , document : String
    , submission_timestamp : Date.Date
    , payment_address : String
    , token : Maybe String
    , owner : Maybe String
    , block_number : Maybe Int
    , block_timestamp : Maybe Date.Date
    }


emptyDocumentReport : DocumentReport
emptyDocumentReport =
    { status = DocSubmitted
    , document = ""
    , submission_timestamp = Date.fromTime 0
    , payment_address = ""
    , token = Nothing
    , owner = Nothing
    , block_number = Nothing
    , block_timestamp = Nothing
    }


decodeTimestamp : String -> Json.Decoder Date.Date
decodeTimestamp s =
    case Date.fromString s of
        Ok d ->
            Json.succeed d

        Err s ->
            Json.fail s

parseStatus : String -> Maybe DocumentStatus
parseStatus s =
    case s of
        "submitted" -> Just DocSubmitted
        "pending" -> Just DocPending
        "registered" -> Just DocRegistered
        _ -> Nothing

documentStatus : Json.Decoder DocumentStatus
documentStatus =
    Json.string
        |> Json.andThen (\s -> parseStatus s
                            |> Maybe.map Json.succeed
                            |> Maybe.withDefault (Json.fail s))

decodeDocumentReport : Json.Decoder DocumentReport
decodeDocumentReport =
    let
        timestamp =
            Json.string
                |> Json.andThen decodeTimestamp
    in
        Json.map8 DocumentReport
            (Json.field "status" documentStatus)
            (Json.field "document" Json.string)
            (Json.field "submission_timestamp" timestamp)
            (Json.field "payment_address" Json.string)
            (Json.maybe (Json.field "token" Json.string))
            (Json.field "owner" (Json.nullable Json.string))
            (Json.field "block_number" (Json.nullable Json.int))
            (Json.field "block_timestamp" (Json.nullable timestamp))


registerDocument : String -> (Result Http.Error DocumentReport -> msg) -> Cmd msg
registerDocument h msgCtor =
    let
        url =
            "/api/docs/" ++ h

        tx =
            Http.request
                { method = "PUT"
                , headers = []
                , url = url
                , body = Http.emptyBody
                , expect = Http.expectJson decodeDocumentReport
                , timeout = Nothing
                , withCredentials = False
                }
    in
        Http.send msgCtor tx


getDocument : String -> (Result Http.Error DocumentReport    -> msg) -> Cmd msg
getDocument h msgCtor =
    let
        url =
            "/api/docs/" ++ h

        tx =
            Http.get url decodeDocumentReport
    in
        Http.send msgCtor tx
