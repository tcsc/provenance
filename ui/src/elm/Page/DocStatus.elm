module Page.DocStatus
    exposing
        ( Model
        , Msg
        , init
        , update
        , view
        , select
        , deselect
        )

import Debug
import Http
import Html exposing (..)
import Time exposing (second)
import Delay
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block

import Api


type alias Model =
    { hash : String
    , report : Maybe Api.DocumentReport
    }


type Msg
    = SelectDoc String
    | DocumentUpdate
    | GetDocumentComplete (Result Http.Error Api.DocumentReport)
    | OnDelayElapsed String


init : Model
init =
    { hash = ""
    , report = Nothing
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case Debug.log "DocStatus" msg of
        SelectDoc hash ->
            onSelectDoc hash model

        GetDocumentComplete result ->
            onGetDocumentComplete result model

        OnDelayElapsed hash ->
            onTimerElapsed hash model

        _ ->
            ( model, Cmd.none )


view : Model -> Html Msg
view model =
    div []
        [ h4 []
            [ text "Document Info"
            , Card.config []
                |> Card.header []
                    [ text  model.hash ]
                |> Card.block []
                    (docInfoView model.report)
                |> Card.view
            ]
        ]

docInfoView : Maybe Api.DocumentReport -> List (Block.Item Msg)
docInfoView report =
    case report of
        Nothing -> [Block.text [] [text "loading..."]]
        Just r -> [ Block.text [] [text "Loaded!"] ]

select : String -> Model -> ( Model, Cmd Msg )
select hash model =
    update (SelectDoc hash) model

{-| The page is being navigated away from. Make sure we stop polling the
    API for document updates, etc.
 -}
deselect : Model -> (Model, Cmd Msg)
deselect model =
    let
        m = { model | hash = ""
            , report = Nothing
            }
    in
        (m, Cmd.none)

onSelectDoc : String -> Model -> ( Model, Cmd Msg )
onSelectDoc hash model =
    let
        newModel =
            { model
                | hash = hash
                , report = Nothing
            }
    in
        beginGetDocument hash newModel


onGetDocumentComplete :
    Result Http.Error Api.DocumentReport
    -> Model
    -> ( Model, Cmd Msg )
onGetDocumentComplete result model =
    case result of
        Ok doc ->
            if doc.document == model.hash then
                let
                    m =
                        { model | report = Just doc }

                    cmd =
                        Delay.after 1 second (OnDelayElapsed model.hash)
                in
                    ( m, cmd )
            else
                ( model, Cmd.none )

        Err e ->
            ( model, Cmd.none )


beginGetDocument : String -> Model -> ( Model, Cmd Msg )
beginGetDocument hash model =
    let
        _ = Debug.log "Fetching" hash
        cmd =
            Api.getDocument hash GetDocumentComplete
    in
        ( model, cmd )


onTimerElapsed : String -> Model -> ( Model, Cmd Msg )
onTimerElapsed hash model =
    if hash /= model.hash then
        ( model, Cmd.none )
    else
        beginGetDocument hash model