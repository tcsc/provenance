module Page.Register
    exposing
        ( Model
        , Msg
        , init
        , subscriptions
        , update
        , view
        )

import Api
import Debug
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Task
import FileReader exposing (NativeFile, FileContentArrayBuffer)
import Maybe
import Ports
import Bootstrap.Button as Button
import Bootstrap.Form as Form
import Bootstrap.Form.Input as Input
import Bootstrap.Card.Block as Block
import Bootstrap.Card as Card
import Bootstrap.Tab as Tab
import Bootstrap.Modal as Modal
import QRCode
import QRCode.ECLevel as ECLevel


type State
    = Idle
    | LoadingContent
    | HashingContent
    | Submitting


type alias Model =
    { state : State
    , fileHash : Maybe String
    , tabState : Tab.State
    , file : Maybe NativeFile
    , error : Maybe String
    , modalVisibility : Modal.Visibility
    , selectedDoc : Maybe Api.DocumentReport
    , receipt : Maybe QRCode.QRCode
    }


type alias FileReadResult =
    Result FileReader.Error FileContentArrayBuffer


type Msg
    = SetDoc String
    | AnimateTab Tab.State
    | AnimateModal Modal.Visibility
    | FileDrop (List NativeFile)
    | FileContent FileReadResult
    | HashReady String
    | SubmitFileHash
    | HashSubmissionComplete (Result Http.Error Api.DocumentReport)
    | CloseRegistrationModal


init : Model
init =
    { state = Idle
    , fileHash = Nothing
    , tabState = Tab.initialState
    , file = Nothing
    , error = Nothing
    , selectedDoc = Nothing
    , modalVisibility = Modal.hidden
    , receipt = Nothing
    }


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Tab.subscriptions model.tabState AnimateTab
        , Modal.subscriptions model.modalVisibility AnimateModal
        , Ports.fileHashed HashReady
        ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case Debug.log "Page.main update" msg of
        AnimateTab s ->
            ( { model | tabState = s }, Cmd.none )

        AnimateModal v ->
            ( { model | modalVisibility = v }, Cmd.none )

        FileDrop files ->
            onFileDrop files model

        FileContent file ->
            onFileContent file model

        HashReady h ->
            ( { model | fileHash = Just h, state = Idle }, Cmd.none )

        SubmitFileHash ->
            case model.fileHash of
                Just h ->
                    submitHash h model

                Nothing ->
                    Debug.crash "Invalid state" model

        HashSubmissionComplete result ->
            onFileSubmissionComplete result model

        CloseRegistrationModal ->
            ( { model | modalVisibility = Modal.hidden }, Cmd.none )

        _ ->
            ( model, Cmd.none )


onFileDrop : List NativeFile -> Model -> ( Model, Cmd Msg )
onFileDrop files model =
    case files of
        -- we only want to deal with one file.
        [ f ] ->
            let
                new_model =
                    { model
                        | file = Just f
                        , state = LoadingContent
                        , fileHash = Nothing
                    }

                cmd =
                    getFileContents f
            in
                ( new_model, cmd )

        _ ->
            ( model, Cmd.none )


onFileContent : FileReadResult -> Model -> ( Model, Cmd Msg )
onFileContent buffer model =
    case buffer of
        Ok data ->
            let
                m =
                    { model | state = HashingContent }

                cmd =
                    Ports.hashFile data
            in
                ( m, cmd )

        Err err ->
            ( { model | error = Just "File load failed" }, Cmd.none )


encodeReceipt : String -> Maybe QRCode.QRCode
encodeReceipt r =
    QRCode.encodeWithECLevel r ECLevel.L
        |> Result.toMaybe


onFileSubmissionComplete :
    Result Http.Error Api.DocumentReport
    -> Model
    -> ( Model, Cmd Msg )
onFileSubmissionComplete result model =
    case result of
        Ok report ->
            let
                new_model =
                    { model
                        | selectedDoc = Just report
                        , receipt =
                            report.token
                                |> Maybe.andThen encodeReceipt
                        , modalVisibility = Modal.shown
                        , state = Idle
                    }
            in
                ( new_model, Cmd.none )

        Err error ->
            ( model, Cmd.none )


submitHash : String -> Model -> ( Model, Cmd Msg )
submitHash hash model =
    let
        cmd =
            Api.registerDocument hash HashSubmissionComplete

        newModel =
            { model | state = Submitting }
    in
        ( newModel, cmd )


getFileContents : NativeFile -> Cmd Msg
getFileContents file =
    FileReader.readAsArrayBuffer file.blob
        |> Task.attempt FileContent


fileTab : Model -> Html Msg
fileTab model =
    let
        filename =
            model.file
                |> Maybe.map (\f -> f.name)
                |> Maybe.withDefault "Pick a file..."

        hash =
            model.fileHash
                |> Maybe.withDefault ""

        enable_file_picker =
            disabled (model.state /= Idle)

        enable_submit_button =
            model.fileHash
                |> Maybe.map (\_ -> False)
                |> Maybe.withDefault True
                |> Button.disabled

        form =
            Form.form []
                [ Html.div [ class "custom-file" ]
                    [ input
                        [ type_ "file"
                        , class "custom-file-input"
                        , id "file_input"
                        , enable_file_picker
                        , FileReader.onFileChange FileDrop
                        ]
                        []
                    , label [ class "custom-file-label", for "file_input" ]
                        [ text filename ]
                    , small [ class "form-text", class "text-muted" ]
                        [ text "File will NOT be uploaded" ]
                    ]
                , Form.group []
                    [ Input.text
                        [ Input.id "file-hash-display"
                        , Input.readonly True
                        , Input.value hash
                        , Input.placeholder "File hash"
                        ]
                    ]
                , Button.button
                    [ Button.primary
                    , Button.onClick SubmitFileHash
                    , enable_submit_button
                    ]
                    [ text "Register" ]
                ]
    in
        Card.config []
            |> Card.block []
                [ Block.titleH4 [] [ text "Select Document" ]
                , Block.text [] [ form ]
                ]
            |> Card.view


hashTab : Model -> Html Msg
hashTab model =
    Card.config []
        |> Card.block []
            [ Block.titleH4 [] [ text "Paste Hash" ]
            , Block.custom <| text "???"
            ]
        |> Card.view


jumbotron : Html Msg
jumbotron =
    Html.div [ class "jumbotron" ]
        [ Html.div [ class "container" ]
            [ h1 [] [ text "Provenance" ]
            , p [] [ text "A proof-of-existence service backed by the Ethereum blockchain." ]
            ]
        ]


receiptDescription : String -> Html Msg
receiptDescription hash =
    let
        docInfoUrl =
            "#/doc/" ++ hash
    in
        div []
            [ p []
                [ text """
                    Your document has been registered in our database and is
                    ready for submission to the Ethereum blockchain. Visit
                    the """
                , a [ href ("#/Document/" ++ hash) ] [ text "Document Info" ]
                , text " page for further instructions."
                ]
            , p []
                [ text """
                    This barcode is is your secret receipt. You can use it to
                    prove that it you registered the document even if you lose
                    the document itself. See the FAQ for more info."""
                ]
            , p []
                [ text """
                    Save it, take a  picture of it, archive it somewhere - but
                    keep whatever you do; it secret, keep it safe."""
                ]
            , hr [] []
            , h4 [] [ text "What next?" ]
            , p []
                [ ol []
                    [ li []
                        [ text "Archive the document! See the FAQ for why."
                        ]
                    , li []
                        [ text "Register the document in the Ethereum blockchain."
                        ]
                    ]
                ]
            ]


{-| Describes the popup dialog that shows the document registration info on success
    information.
-}
registrationModal : Model -> Html Msg
registrationModal model =
    let
        hash =
            model.selectedDoc
                |> Maybe.map (\d -> d.document)
                |> Maybe.withDefault ""
    in
        Modal.config CloseRegistrationModal
            |> Modal.withAnimation AnimateModal
            |> Modal.h3 [] [ text "Submission succesful" ]
            |> Modal.large
            |> Modal.body []
                [ p []
                    [ div [ class "media" ]
                        [ div [ class "align-self-start", class "mr-3" ]
                            [ model.receipt
                                |> Maybe.map QRCode.toCanvas
                                |> Maybe.withDefault (text "")
                            ]
                        , div [ class "media-body" ]
                            [ receiptDescription hash
                            ]
                        ]
                    ]
                ]
            |> Modal.footer []
                [ Button.button
                    [ Button.outlinePrimary
                    , Button.attrs [ onClick CloseRegistrationModal ]
                    ]
                    [ text "Close" ]
                ]
            |> Modal.view model.modalVisibility


view : Model -> Html Msg
view model =
    Html.div []
        [ jumbotron
        , registrationModal model
        , Tab.config AnimateTab
            |> Tab.withAnimation
            |> Tab.items
                [ Tab.item
                    { id = "tabFile"
                    , link = Tab.link [] [ text "Upload File" ]
                    , pane = Tab.pane [] [ fileTab model ]
                    }
                , Tab.item
                    { id = "tabHash"
                    , link = Tab.link [] [ text "Hash" ]
                    , pane = Tab.pane [] [ hashTab model ]
                    }
                ]
            |> Tab.view model.tabState
        ]
