// pull in desired CSS/SASS files
require( './styles/main.scss' );

const asmCrypto = require('asmcrypto-lite');

//import 'bootstrap';

// inject bundled Elm app into div#main
var Elm = require( '../elm/Main' );

var app = Elm.Main.embed( document.getElementById( 'main' ) );

app.ports.hashFile.subscribe(function(buffer) {
    var hash = asmCrypto.SHA256.hex(buffer);
    app.ports.fileHashed.send(hash);
})
