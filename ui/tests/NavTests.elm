module NavTests exposing (..)

import Debug
import Test exposing (..)
import Fuzz exposing (string)
import Expect
import Navigation

import Nav

hashLoc : String -> Navigation.Location
hashLoc hash =
    { href = "https://example.com:80/" ++ hash
    , host = "example.com:80"
    , hostname = "example.com"
    , protocol = "https"
    , origin = "https://example.com:80/"
    , port_ = "80"
    , pathname = "/"
    , search = ""
    , hash = hash
    , username = ""
    , password = ""
    }


navigationParsing : Test
navigationParsing =
    describe "The URL parser"
        [ test "Maps the empty hash to register" <|
            \_ -> hashLoc ""
                |> Nav.parseUrl
                |> Expect.equal (Just Nav.Register)
        , test "Maps the prefix hash to register" <|
             \_ -> hashLoc "#"
                 |> Nav.parseUrl
                 |> Expect.equal (Just Nav.Register)
        , test "Maps doc to doc list" <|
             \_ -> hashLoc "#/doc"
                 |> Nav.parseUrl
                 |> Expect.equal (Just Nav.DocumentList)
        , test "Maps doc with trailing hash to doc list" <|
             \_ -> hashLoc "#/doc/"
                 |> Nav.parseUrl
                 |> Expect.equal (Just Nav.DocumentList)
        , test "Maps doc with hash to individual doc" <|
             \_ -> hashLoc "#/doc/AABBCCDDEEFF"
                 |> Nav.parseUrl
                 |> Expect.equal (Just (Nav.Document "AABBCCDDEEFF"))
        , test "FAQ maps to faq" <|
            \_ -> hashLoc "#/faq"
                |> Nav.parseUrl
                |> Expect.equal (Just Nav.Faq)
        , test "Gibberish fails parser" <|
            \_ -> hashLoc "#l;kdahpadn"
                |> Nav.parseUrl
                |> Expect.equal Nothing
        ]
