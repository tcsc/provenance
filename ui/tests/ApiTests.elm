module ApiTests exposing (..)

import Test exposing (..)
import Expect
import Json.Decode as Json
import Date
import Result
import Api


submission_ts =
    Date.fromString "2018-04-05T12:52:24.427363Z"
        |> unwrap


suite : Test
suite =
    describe "The API module"
        [ describe "Document report parser"
            [ test "Parses a report with block info" <|
                \_ ->
                    let
                        block_ts =
                            Date.fromString "2018-04-05T11:22:33.456Z"
                                |> unwrap
                        text =
                            """
                        {
                            "document": "ABCFEFABCDEFABCDEF",
                            "status": "submitted",
                            "owner": null,
                            "payment_address": "0x552c61aa80440034343d47b518e1ad4d16a976a6",
                            "submission_timestamp": "2018-04-05T12:52:24.427363Z",
                            "block_number": 42,
                            "block_timestamp": "2018-04-05T11:22:33.456Z",
                            "token": "11223344556677889900AABBCCDDEEFF"
                        }"""

                        expected =
                            { status = Api.DocSubmitted
                            , document = "ABCFEFABCDEFABCDEF"
                            , submission_timestamp = submission_ts
                            , payment_address = "0x552c61aa80440034343d47b518e1ad4d16a976a6"
                            , token = Just "11223344556677889900AABBCCDDEEFF"
                            , owner = Nothing
                            , block_number = Just 42
                            , block_timestamp = Just block_ts
                            }
                    in
                        Json.decodeString Api.decodeDocumentReport text
                            |> unwrap
                            |> Expect.equal expected

            , test "Parses a response with token" <|
                \_ ->
                    let
                        text =
                            """
                        {
                            "document": "ABCFEFABCDEFABCDEF",
                            "status": "submitted",
                            "owner": null,
                            "payment_address": "0x552c61aa80440034343d47b518e1ad4d16a976a6",
                            "block_number": null,
                            "block_timestamp": null,
                            "submission_timestamp": "2018-04-05T12:52:24.427363Z",
                            "token": "11223344556677889900AABBCCDDEEFF"
                        }"""

                        expected =
                            { status = Api.DocSubmitted
                            , submission_timestamp = submission_ts
                            , document = "ABCFEFABCDEFABCDEF"
                            , payment_address = "0x552c61aa80440034343d47b518e1ad4d16a976a6"
                            , token = Just "11223344556677889900AABBCCDDEEFF"
                            , owner = Nothing
                            , block_number = Nothing
                            , block_timestamp = Nothing
                            }
                    in
                        Json.decodeString Api.decodeDocumentReport text
                            |> unwrap
                            |> Expect.equal expected

            , test "Parses a response with no token" <|
                \_ ->
                    let
                        text =
                            """
                        {
                          "status": "submitted",
                          "document": "ABCFEFABCDEFABCDEF",
                          "owner": null,
                          "payment_address": "0x552c61aa80440034343d47b518e1ad4d16a976a6",
                          "block_number": null,
                          "block_timestamp": null,
                          "submission_timestamp": "2018-04-05T12:52:24.427363Z"
                        }"""
                        expected =
                            { status = Api.DocSubmitted
                            , document = "ABCFEFABCDEFABCDEF"
                            , submission_timestamp = submission_ts
                            , payment_address = "0x552c61aa80440034343d47b518e1ad4d16a976a6"
                            , token = Nothing
                            , owner = Nothing
                            , block_number = Nothing
                            , block_timestamp = Nothing
                            }
                    in
                        Json.decodeString Api.decodeDocumentReport text
                            |> unwrap
                            |> Expect.equal expected
            ]

        , describe "The document status parser"
            [ test "parses submitted" <| \_ ->
                Json.decodeString Api.documentStatus "\"submitted\""
                    |> unwrap
                    |> Expect.equal Api.DocSubmitted
            , test "parses pending" <| \_ ->
                Json.decodeString Api.documentStatus "\"pending\""
                    |> unwrap
                    |> Expect.equal Api.DocPending
            , test "parses registered" <| \_ ->
                Json.decodeString Api.documentStatus "\"registered\""
                    |> unwrap
                    |> Expect.equal Api.DocRegistered
            ]
        ]

unwrap : Result e a -> a
unwrap r =
    case r of
        Ok v ->
            v

        Err e ->
            Debug.crash "Expected success: " e
